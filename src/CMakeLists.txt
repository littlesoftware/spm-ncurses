cmake_minimum_required(VERSION 3.5)

project(spm-ncurses LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../include")

set( SOURCES
    NcursesView.cpp
)

set( HEADERS
    ${INCLUDE_DIR}/NcursesView.h
)

set( LIBS
    ncursesw
    panelw
    formw
    spm-core
)

find_package(spm-core REQUIRED)

set(LIBRARY_TYPE STATIC)
if(BUILD_SHARED)
    set(LIBRARY_TYPE SHARED)
endif()
add_library(${PROJECT_NAME} ${LIBRARY_TYPE} ${SOURCES} ${HEADERS})
target_link_libraries(${PROJECT_NAME} ${LIBS})
target_include_directories(${PROJECT_NAME} PUBLIC ../include)
